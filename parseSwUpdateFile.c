#include <stdio.h>
#include <stdlib.h>

/**
 * Author:  Alberto Portillo
 * Date:    03/19/2021
 * Note:    Use this file to add your specific method to parse data from the 
 *          bin SW update file to usable packets to send over the CAN bus 
 *          to update the CAN device. 
 *          
 *          Place the update file in the same folder as this file.
 **/

// Formated to read ST *.bin update files
void parse_ST_SwUpdateFile()
{

}

void getFile()
{

}

// TODO: Add your custom update code here. 
// i.e. void parse_TI_SwUpdateFile() for TI chips