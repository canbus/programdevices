# the compiler: gcc for C program, define as g++ for C++
CC = gcc

# compiler flags:
#  -g    adds debugging information to the executable file
#  -Wall turns on most, but not all, compiler warnings
CFLAGS  = -g -Wall

# the build target executable:
TARGET = canProgram

default: $(TARGET)

# To create the executable file count we need the object files
# main.o:
#
$(TARGET): main.o
	$(CC) $(CFLAGS) -o $(TARGET) main.c
 
# To start over from scratch, type 'make clean'.  This
# removes the executable file, as well as old .o object
# files and *~ backup files:
#
clean :
	$(RM) canProgram *.o
